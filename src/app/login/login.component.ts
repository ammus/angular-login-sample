import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {UserService} from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username:string = '';
  password:string = '';

  constructor(private router:Router, private user:UserService) {

  }

  ngOnInit() {
  }
  login(){
    if(this.username == 'admin' && this.password == 'admin')
    {
      this.user.setLoggedIn();
      this.router.navigate(['dashboard']);
    }
    else if(this.username == '' && this.password == '')
    {
        alert('Please enter your login credentials!')
    }
    else
    {
      alert('Invalid credentials!')
    }
  };

}
