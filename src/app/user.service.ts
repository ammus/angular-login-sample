import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

  private isLoggedIn;

  constructor() {
    this.isLoggedIn = false;
  }
  setLoggedIn(){
    this.isLoggedIn = true;
  }
  getLoggedIn(){
    return this.isLoggedIn;
  }

}
