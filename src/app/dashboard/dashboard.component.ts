import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { DataService } from '../services/data.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userData:user[];

  constructor(private user:UserService, private dataService:DataService) { }

  ngOnInit() {
    this.dataService.getData().subscribe((data) => {
      this.userData = data;
      console.log(data)
    });
  }

}
interface user{
  id:number,
  name:string,
  email:string,
  phone:number
}
